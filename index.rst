.. KumaGravity documentation master file, created by
   sphinx-quickstart on Sat Nov 19 16:35:14 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

==================================================
くまのための相対論
==================================================

特殊相対性理論、一般相対性理論を学習したときのノート（不完全）です。
教科書には、講談社基礎物理学シリーズ9「相対性理論」（著：杉山直）、を使用しました。

.. toctree::
   :maxdepth: 2

   sugiyama/index.rst
   tips/index.rst


教科書・参考書
==================================================

- 講談社　基礎物理学シリーズ９　「相対性理論」　杉山直（2010）
- https://bookclub.kodansha.co.jp/product?item=0000149596

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
