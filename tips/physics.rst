==================================================
physicsパッケージを利用する
==================================================

物理学で使う記号は ``physics`` パッケージを使うととても楽になる。
``\mathrm{d} x \mathrm{d} y`` が ``\dd{x} \dd{y}`` で書けたり、
``\frac{ \partial f}{ \partial x}}`` が ``\pdv{f}{x}`` で書けたりする。

パッケージを利用するには ``LaTeX`` 出力と ``HTML`` 出力をそれぞれ設定する必要がある。

表示の確認
==================================================


.. code-block:: rst
    :caption: 微小量の記号

    .. math::

        \dd{x} \quad \dd[2]{x} \quad \dd{t^2}


.. math::

    \dd{x} \quad \dd[2]{x} \quad \dd{t^2}



.. code-block:: rst
    :caption: ベクトルなどの記号

    .. math::

        \abs{a} \quad \vb{F} \quad \va{F}


.. math::

    \abs{a} \quad \vb{F} \quad \va{F}


.. code-block:: rst
    :caption: ベクトル解析の記号

    .. math::

        \grad{\Phi} \quad \laplacian{\Phi} \quad \div{\vb{E}} \quad \curl{\vb{B}}

.. math::

    \grad{\Phi} \quad \laplacian{\Phi} \quad \div{\vb{E}} \quad \curl{\vb{B}}


.. code-block:: rst
    :caption: 全微分（derivative）の記号

    .. math::

        \dv{x} \quad \dv{f}{x} \quad \dv[n]{f}{x}

.. math::

    \dv{x} \quad \dv{f}{x} \quad \dv[n]{f}{x}


.. code-block:: rst
    :caption: 偏微分（partial derivative）の記号

    .. math::

        \pdv{x} \quad \pdv{f}{x} \quad \pdv[n]{f}{x}

.. math::

    \pdv{x} \quad \pdv{f}{x} \quad \pdv[n]{f}{x}


LaTeX出力の設定
==================================================

``make latexpdf`` でビルドしたTeXファイルのプリアンブルに ``\usepackage{physics}`` を追加したい。
このようにパッケージを追加する時は ``conf.py`` の ``latex_elements['extrapackages']`` に設定を追記する。
``extrapackages`` に記述する文字列は ``raw`` 文字列にしておいた。

.. code-block:: python

    latex_elements = {

        'preamble': '',

        'extrapackages' : r'''
        \usepackage{physics}
        ''',

    }


HTML出力の設定
==================================================

``make html`` でビルドしたHTMLファイルの数式表示にはデフォルトで ``MathJax`` が使われている。

.. code-block:: html

    <script async="async" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-AMS-MML_HTMLorMML"></script>


.. tip::

    - 現在のSphinxのバージョン（v3.4）で使っているのは ``MathJax 2.7.7``
    - 次のバージョン（v4）では ``MathJax 3`` に変更されるらしい


まず ``MathJax`` の ``physics`` パッケージが使えることを確認した。
すると、 ``physics`` パッケージは他のコマンドを書き換えたりするので **デフォルトでは無効化** されていることが分かった。

次に ``Sphinx`` のドキュメントで ``MathJax`` の設定方法を確認した。
``MathJax Extension`` の設定項目を確認し ``mathjax_config`` に設定を書けばよいことが分かった。

.. code-block:: python

    mathjax_config = {
        'extensions': ['physics'],
    }

.. code-block:: html

    <script async="async" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <script type="text/x-mathjax-config">MathJax.Hub.Config({"extensions": ["physics"]})</script>


.. todo::

    - これで ``physics`` パッケージが使えるようになったはずなのに、全然ダメ
    - それどころか、他の数式も表示されなくなってしまったので、HTML出力は一旦諦めることにする


参考サイト
==================================================

- https://www.sphinx-doc.org/ja/master/latex.html
- https://www.sphinx-doc.org/ja/master/usage/extensions/math.html
- https://docs.mathjax.org/en/latest/input/tex/extensions/physics.html
- https://docs.mathjax.org/en/latest/input/tex/macros/index.html
- https://docs.mathjax.org/en/v2.7-latest/tex.html#supported-latex-commands
